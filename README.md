### What is this repository for? ###

This repo is an appliance task made to apply to Lognex company, Java-junior position.

* Assembling
* Run
* Read data with from file using pipeline
* Read data with CLI

### Build the project ###

* Install Maven3
* Binaries: http://maven.apache.org/download.html
* Clone the project 
```
#!git

git clone https://lopuhan@bitbucket.org/lopuhan/juniorapp.git
```
or download sources manually from bitbucket.org, unzip them
* Navigate to the folder with sources
* To build executables, run: `mvn install`

### Run ###

* Navigate to the /target directory
* Open the terminal (if you haven't already)
* Type `java -jar JuniorApp-1.0-SNAPSHOT-jar-with-dependencies.jar`
* ????

### Read data from file using pipeline ###

* Add ` < %FILENAME%` to above mentioned command
* Press 'Enter'
* Get result

### Read data with CLI ###

* Just press 'Enter' after typing `java -jar JuniorApp-1.0-SNAPSHOT-jar-with-dependencies.jar` in the command line
* You can add dependencies by typing `%id1% %id2%` and pressing 'Enter'. App will notify you if it had read the dependency successfully or failed
* When you've finished adding dependencies, type `calc`, press 'Enter'
* Get result