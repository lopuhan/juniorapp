/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juniorapp;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

/**
 *
 * @author Lopuhan
 */
public class Dependencies {
    HashMap<Long, Long> dependencies = new HashMap<>();
    ArrayList<Long> selfDeps = new ArrayList<>();
    
    public synchronized void readAndCalculate(Scanner sc) throws InvalidParameterException {
        readFile(sc);
        calculate();
    }
    
    private void calculate() { 
    
        if (dependencies.isEmpty()) {
            return;
        }
        
    //since one unique key can only be associated with only one entry, no dependency can 'escape' the cycle
    //if key leads to the cycle, it cannot lead the chain to another cycle
        ArrayList<Long> checkedKeys = new ArrayList<>();
        Iterator<Long> keysIter = dependencies.keySet().iterator();
        long temp, current;
        
        do {
            current = keysIter.next();
            if(checkedKeys.contains(current)) 
                continue;
            
            temp = current;
            
            do {
                if(!checkedKeys.contains(temp)) {
                    checkedKeys.add(temp);
                }
                
                temp = dependencies.get(temp);
                
                if(checkedKeys.contains(temp))  {                    
                    selfDeps.add(temp);                    
                    break;
                }
                    
            } while (dependencies.containsKey(temp));            
            
        } while (keysIter.hasNext());
    }
    
    private void readFile(Scanner sc) throws InvalidParameterException {
        int lineCount = 0;
        while(sc.hasNextLine()) {
            lineCount++;
            try {
                readLine(sc.nextLine(), dependencies);
            }
            catch(Exception e) {
                System.out.println(String.format("%s at line %s: line skipped", e.getMessage(), lineCount));
            }
            
        }
    }
    
    private void readLine(String line, HashMap<Long, Long> deps) 
      throws InvalidParameterException {        
        String[] numbers = line.split(" ");
        if(line.equals("calc")){ calculate(); System.out.print(resultsToString()); return; }
        try
        {
            deps.put(Long.parseLong(numbers[0]), Long.parseLong(numbers[1]));
            System.out.println(String.format("Read dependency: %s -> %s", numbers[0],numbers[1]));
        }
        catch(Exception e) {
            throw new InvalidParameterException("Invalid line read");
        }
    }
    
    public synchronized String resultsToString() {
        StringBuilder sb = new StringBuilder("Found cycle dependencies:\n");
        
        for(long l : selfDeps) {
            long curNumber = l;
            sb.append(Long.toString(l));
            do {
                curNumber = dependencies.get(curNumber);
                sb.append(" ");
                sb.append(Long.toString(curNumber));
            } while (curNumber != l);
            sb.append("\r\n");
        }
        return sb.toString();
    }
}
