/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package juniorapp;

import java.util.Scanner;

/**
 *
 * @author Lopuhan
 */
public class JuniorApp {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) { 
        Scanner scanner = new Scanner(System.in);
        Dependencies deps = new Dependencies();
        deps.readAndCalculate(scanner);        
        System.out.print(deps.resultsToString());
    }  
    
}
